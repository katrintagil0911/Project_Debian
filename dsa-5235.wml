#use wml::debian::translation-check translation="3508f9a531af1aca0b0e166eb9e274f958f07db9"
<define-tag description>Обновление безопасности</define-tag>
<define-tag moreinfo>
<p>В BIND, реализации DNS-сервера, было обнаружено несколько уязвимостей.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

    <p>Иегуда Афек, Анат Бремлер-Барр и Шани Стейнрод обнаружили, что
    ошибка в коде распознавателя может привести к тому, что named потратит чрезмерные суммы
    времени на обработку больших делегаций, что значительно снижает производительность
    распознавателя и приводит к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3080">CVE-2022-3080</a>

    <p>Максим Одининцев обнаружил, что распознаватель может аварийно завершать работу, когда устаревший
    кэш и устаревшие ответы включены с нулевым значением
    устаревший stale-answer-timeout. Удаленный злоумышленник может воспользоваться этим
    недостатком, чтобы возбудить отказ в обслуживании (сбой daemon) с помощью специальнольно
    обработанных запросы к распознавателю.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

    <p>Было обнаружено, что проверочный код DNSSEC для алгоритма ECDSA
    подвержен уязвимости утечки памяти. Удаленный злоумышленник
    может воспользоваться этим недостатком, чтобы заставить BIND потреблять ресурсы,
    что приведет к отказу в обслуживании.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

    <p>Было обнаружено, что проверочный код DNSSEC для алгоритма EdDSA
    подвержен уязвимости утечки памяти. Удаленный злоумышленник
    может воспользоваться этим недостатком, чтобы заставить BIND потреблять ресурсы,
    что приведет к отказу в обслуживании.</p></li>

</ul>

<p>В стабильном дистрибутиве (bullseye), эти проблемы были исправлены в
версии 1:9.16.33-1~deb11u1.</p>

<p>Рекомендуется обновить пакеты bind9.</p>

<p>Для получения подробной информации о статусе безопасности bind9 обратитесь к его
странице отслеживания безопасности по ссылке:
<a href="https://security-tracker.debian.org/tracker/bind9">https://security-tracker.debian.org/tracker/bind9</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5235.data"
